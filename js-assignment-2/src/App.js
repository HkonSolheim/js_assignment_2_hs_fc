import './App.scss';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom'
import Login from './views/login';
import Translations from './views/translations';
import Profile from './views/profile';
function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Routes>
      <Route path='/' element={<Login/>} />
      <Route path='/translations' element={<Translations/>} />
      <Route path='/profile' element={<Profile/>} />
    </Routes>
    </div>
    </BrowserRouter>
    
  );
}

export default App;
