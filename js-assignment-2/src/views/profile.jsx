import withAuth from "../hoc/withAuth"

const Profile = () => {
    return(
        <h1>Profile</h1>
    )
}

export default withAuth(Profile)