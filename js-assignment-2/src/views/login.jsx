import LoginForm from "../components/login/LoginForm";
import logo from "../imgs/logo.png";

const Login = () => {
  return (
    <>
      <div className="container bg-secondary p-5 mx-auto">
        {
          // Website logo
        }
        <div className="row pb-5 w-100 mx-auto">
          <div className="col text-center">
            <img
              className="img-fluid"
              src={logo}
              alt="logo of translate page"
            />
          </div>
        </div>
        <hr />
        {
          // User login input field
        }
        <div id="user-login-bar" className="row">
          <div className="col d-flex justify-content-center">
            <LoginForm />
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
