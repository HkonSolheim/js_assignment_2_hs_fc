import { createContext, useContext, useState } from "react";
import { Storage_KEY_USER } from "../const/StorageKeys";
import { storageRead } from "../utils/storage";
//Context -> exposes state
const UserContext = createContext()
export const useUser= () => {
    return useContext(UserContext) // { user, setUser}
}
//Provider -> manages state
const UserProvider = ({children}) => {
    const [user, setUser] = useState(storageRead(Storage_KEY_USER))
    const state = {
        user,
        setUser,
    }
    return(
        <UserContext.Provider value={ state }>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider