import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { Storage_KEY_USER } from "../../const/StorageKeys";
import { HiArrowRightCircle } from "../../../node_modules/react-icons/hi2";
import { HiUser } from "../../../node_modules/react-icons/hi2";

const userNameConfig = {
  required: true,
  minLength: 5,
};

const LoginForm = () => {
  //hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  //Local State
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  //Side Effects
  useEffect(() => {
    if (user !== null) {
      navigate("translations");
    }
  }, [user, navigate]);
  //Event Handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }

    if (userResponse !== null) {
      storageSave(Storage_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };
  //Render Functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }
    if (errors.username.type === "minLength") {
      return <span>Username is too short, must be 10 characters</span>;
    }
  })();

  return (
    <>
      <div className="flex-col">
        <form
          className="d-flex flex-row flex-nowrap justify-content-between border bg-white rounded-5"
          onSubmit={handleSubmit(onSubmit)}
        >
          <fieldset>
            <div className="my-auto p-2">
              <label htmlFor="username"></label>
              <HiUser size={25} /> |
              <input
                type="text"
                className="border-0"
                placeholder="What is your name?"
                {...register("username", userNameConfig)}
              />
            </div>
          </fieldset>

          <button
            id="btn submit"
            type="submit"
            form="input-form"
            className="btn btn-white text-primary"
            disabled={loading}
          >
            <HiArrowRightCircle size={35} />
            Log in
          </button>
        </form>
        <div className="$danger">
          {errorMessage}
          {loading && <p>Logging in...</p>}
          {apiError && <p> {apiError}</p>}
        </div>
      </div>
    </>
  );
};
export default LoginForm;
