import { HiArrowRightCircle } from "react-icons/hi2";
import { HiUser } from "react-icons/hi2";

 

const SearchBar = ({onChange}) =>  {
return (<>
        <div className="d-flex flex-row flex-nowrap justify-content-between border bg-white rounded-5">
            <div className="my-auto p-2">
                <HiUser size={25} /> |
            </div>
            <div className="my-auto ">
                <input onChange={onChange} className="border-0" placeholder="What is your name?"></input>
            </div>
            <div>
                <button type="button" className="btn btn-white text-primary">
                    <HiArrowRightCircle size={35} />
                </button>
            </div>
        </div>
        </>)
}

export default SearchBar; 