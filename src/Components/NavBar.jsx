import logo from "../resources/logo.png"

const NavBar = (props) => {
    return (
        <>
            <nav className="navbar navbar-light bg-secondary p-5">
                <div className="container d-flex-sm">
                    <img src={logo} className="img-fluid" alt="'Lost in Translation' - Translation" />
                </div>
            </nav >
            <hr></hr>
        </>
    );
}

export default NavBar;