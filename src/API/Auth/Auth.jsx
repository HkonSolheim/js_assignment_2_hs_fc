import { Component } from "react";
import { checkForUser } from "../User";
import { Redirect} from "react-dom/client"

const checkAuthorization = Component => props => {
    checkForUser(props.userName).then(canProceed=>{
        return canProceed ? 
            <Component {...props}/> 
        :   <Redirect to="/login"/>
    });
}