import SearchBar from "../Components/SearchBar";
import logo from "../resources/logo.png"

const LoginPage = () => {

    return (
        <div className="container bg-secondary p-5" style={{ border: 1 + 'px red solid' }}>
            {
                // Website logo
            }
            <div className="row pb-5" style={{ border: 1 + 'px Blue solid' }}>
                <div className="col  ">
                    <img className="img-fluid" src={logo} alt="logo of translate page" style={{ border: 1 + 'px green solid' }}/>
                </div>
            </div>

            {
                // User login input field
            }
            <div id="user-login-bar" className="row">
                <div className="col d-flex justify-content-center">
                    <SearchBar onChange={parentHandler}></SearchBar>
                </div>
            </div>
        </div>
    )
}

// Checks user
const parentHandler = event => {
    let value = event.target.value;
    return findUser(value) ? console.log("Welcome " + value + "!") : console.log("Hello " + value + ", please create an account if you want to log in.");
}

const findUser = (_name) => {
    // return ["harald", "roger", "ida", "geir", "kåre", "håkon", "fredrik"].filter(name => name === _name.toLowerCase()).length > 0;

}
export default LoginPage;
