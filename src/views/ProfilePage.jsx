import NavBar from "../Components/NavBar"

const ProfilePage = () => {
    return (
        <>
            <NavBar>
                <h1 className="h1">Profile page</h1>
            </NavBar>
        </>
    )
}
export default ProfilePage;