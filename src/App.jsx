import { BrowserRouter, Routes, Route } from "react-router-dom";
import {LoginPage, TranslationPage, ProfilePage} from "./views";

const App = () => (
    <BrowserRouter>
        <Routes>
            <Route path='/' element={<LoginPage/>} />
            <Route path='/translate' element={<TranslationPage/>} />
            <Route path='/profile' element={<ProfilePage/>} />
        </Routes>
    </BrowserRouter>
);

export default App;
